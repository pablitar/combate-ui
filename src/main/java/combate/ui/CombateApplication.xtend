package combate.ui

import org.uqbar.arena.Application
import combate.CombateAppModel

class CombateApplication extends Application {
	
	override protected createMainWindow() {
		new CombateWindow(this, new CombateAppModel)
	}
	
	def static void main(String[] args) {
		new CombateApplication().start
	}
	
}