package combate.ui

import org.uqbar.arena.windows.SimpleWindow
import combate.CombateAppModel
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.windows.WindowOwner
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.widgets.Button

class CombateWindow extends SimpleWindow<CombateAppModel> {

	new(WindowOwner parent, CombateAppModel model) {
		super(parent, model)
		this.title = "Combate!"
	}

	override protected addActions(Panel actionsPanel) {
		new Button(actionsPanel) => [
			caption = "Comenzar de nuevo"
			onClick [| modelObject.nuevoCombate]
		]
		
		new Button(actionsPanel) => [
			caption = "Reemplazar Muertos"
			onClick [| modelObject.reemplazarMuertos]
			bindEnabledToProperty('algunoMuerto')
		]		
	}

	override protected createFormPanel(Panel formPanel) {
		new Label(formPanel) => [
			text = "Ready... Fight!"
			width = 600
			fontSize = 20
		]

		val panelCombatientes = new Panel(formPanel) => [
			layout = new HorizontalLayout
		]

		new PanelGuerrero(panelCombatientes, 'combatiente', [|modelObject.atacaCombatiente])
		new PanelGuerrero(panelCombatientes, 'retador', [|modelObject.atacaRetador])
	}

}
