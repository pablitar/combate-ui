package combate.ui

import org.uqbar.arena.widgets.Container
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.lacar.ui.model.Action

import static extension org.uqbar.arena.xtend.ArenaXtendExtensions.*
import java.awt.Color
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.List
import combate.Guerrero

class PanelGuerrero extends Panel {

	val String propiedadGuerrero
	static val panelWidth = 300

	new(Container container, String propiedadGuerrero, Action onAttack) {
		super(container)

		this.propiedadGuerrero = propiedadGuerrero

		agregarCampo("Nombre", "nombre")
		agregarCampo("Potencial Ofensivo", "potencialOfensivo")
		agregarCampo("Potencial Defensivo", "potencialDefensivo")
		agregarCampo("Energia", "energia") => [
			background = Color.GREEN
		]

		new Label(this) => [
			text = "Víctimas"
		]
		
		new List(this) => [
			bindItemsToProperty(propiedadGuerrero + ".victimas").adaptWith(Guerrero, "nombre")
			height = 200
		]

		new Button(this) => [
			caption = "Atacar!"
			onClick(onAttack)
			bindEnabledToProperty(propiedadGuerrero + ".estaVivo")
		]
	}

	def agregarCampo(String label, String propiedad) {
		new Label(this) => [
			text = label
		]

		new Label(this) => [
			value <=> propiedadGuerrero + "." + propiedad
			fontSize = 15
			width = panelWidth
		]
	}

}
